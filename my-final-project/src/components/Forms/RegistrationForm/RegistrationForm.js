import './RegistrationForm.css'
import {Link} from "react-router-dom";
import {useState} from "react";

const RegistrationForm = ({isActive}) => {

    const [firstname, setFirstname] = useState('')
    const [firstnameIsEmpty, setFirstnameIsEmpty] = useState(false)

    const [lastname, setLastname] = useState('')
    const [lastnameIsEmpty, setLastnameIsEmpty] = useState(false)

    const [email, setEmail] = useState('')
    const [emailError, setEmailError] = useState('')
    //const [emailExist, setEmailExist] = useState(false)

    const [password, setPassword] = useState('')
    const [passwordError, setPasswordError] = useState('')

    const [checked, setChecked] = useState(false)
    const emptyError = 'The field is required'
    const submitHandler = event => {
        event.preventDefault()
        const newUser = {firstname: firstname,
                        lastname: lastname,
                        email: email,
                        password: password}
        console.log(newUser)
    }

    const nameHandler = (event) => {
        switch (event.target.name) {
            case 'firstname':
                setFirstnameIsEmpty(event.target.value === '')
                setFirstname(event.target.value)
                break;
            case 'lastname':
                setLastnameIsEmpty(event.target.value === '')
                setLastname(event.target.value)
                break;
            default: break;
        }
    }
    const emptyHandler = (event) => {
        if (!event.target.value) {
            switch (event.target.name) {
                case 'email': setEmailError(emptyError); break;
                case 'password': setPasswordError(emptyError); break;
                default: break
            }
        }
    }

    const emailHandler = (event) => {
        const email = event.target.value
        console.log(email)
        if (email && !isValid(email)) {
            setEmailError('Email is not valid')
            // } else if (emailExist) {
            //     setEmailError(prevState => 'This email already exists')
        } else if (!email) {
            setEmailError(emptyError)
        } else {
            setEmail(email)
            //setEmailExist(true)
            setEmailError('')
        }
    }

    const passwordHandler = (event) => {
        const password = event.target.value;
        if (password && !isSecure(password)) {
            setPasswordError('The password should contain at least 8 symbols')
        } else if (!password) {
            setPasswordError('The field is required')
        } else {
            setPassword(event.target.value)
            setPasswordError('')
        }
    }

    const isSecure = (password) => {
        return !(password && password.length < 8);
    }

    const isValid = (email) => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }



    return (
        <div className={isActive === 2 ? 'form visible' :  'invisible'} id={'registration'}>
            <form className='innerForm'>
                <h1 className={'form__title'}> Create an account </h1>
                <div className='input firstnameInput'>
                    <label>
                        <input
                            type='text'
                            placeholder='First name'
                            value={firstname}
                            name = 'firstname'
                            onBlur={event => setFirstnameIsEmpty(event.target.value === '')}
                            onChange={event => nameHandler(event)}
                        />
                    </label>
                    {firstnameIsEmpty && <div>{emptyError}</div>}
                </div>
                <div className='input lastnameInput'>
                    <label>
                        <input type='text'
                               placeholder='Last name'
                               value={lastname}
                               name = 'lastname'
                               onBlur={event => setLastnameIsEmpty(event.target.value === '')}
                               onChange={event => nameHandler(event)}
                        />
                    </label>
                    {lastnameIsEmpty && !lastname && <div>{emptyError}</div>}
                </div>
                <div className='input emailInput'>
                    <label>
                        <input
                            id={'emailReg'}
                            type='text'
                            name={'email'}
                            placeholder='Email'
                            onBlur={event => emptyHandler(event)}
                            onChange={event => emailHandler(event)}
                        />
                    </label>
                    {emailError && <div>{emailError}. {/*{emailExist && <Link to={'/'}>Go to Sign in form</Link>}*/}</div>}

                </div>
                <div className='input passwordInput'>
                    <label >
                        <input id={'passwordReg'}
                               type='password'
                               name={'password'}
                               placeholder='Password'
                               onBlur={event => emptyHandler(event)}
                               onChange={event => passwordHandler(event)}
                        />
                    </label>
                    {passwordError && <div>{passwordError}</div>}
                </div>
                <div className='checkboxWrapper'>
                    <input type='checkbox' id='checkboxAccept' onChange={()=> setChecked(!checked)}/>
                    <label htmlFor='checkboxAccept'> I accept the <Link to={'/agreement'}>Terms and Conditions</Link>
                    </label>
                </div>
                <div className='buttonWrapper'>
                    <button className={'button'}
                            type={"submit"}
                            disabled = {!(!firstnameIsEmpty && !lastnameIsEmpty && !emailError && !passwordError && checked)}
                            onSubmit={event => submitHandler(event)}> Create </button>
                </div>
            </form>
        </div>
    )
}


export default RegistrationForm

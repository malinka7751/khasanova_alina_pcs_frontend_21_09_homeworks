import './SignInForm.css'
import {useEffect, useState} from "react";

const SignInForm = ({isActive, setLoggedIn, loggedIn}) => {
    const [email, setEmail] = useState('')
    const [emailError, setEmailError] = useState('')

    const [password, setPassword] = useState('')
    const [passwordError, setPasswordError] = useState('')

    const [checked, setChecked] = useState(false)
    const emptyError = 'The field is required'

    useEffect(()=>{
        return localStorage.setItem('user', JSON.stringify({email: email, password: password}))
    }, [loggedIn])


    const emptyHandler = (event) => {
        if (!event.target.value) {
            switch (event.target.name) {
                case 'email': setEmailError(emptyError); break;
                case 'password': setPasswordError(emptyError); break;
                default: break
            }
        }
    }

    const emailHandler = (event) => {
        const email = event.target.value
        if (email && !isValid(email)) {
            setEmailError('Email is not valid')
        } else if (!email) {
            setEmailError(prevState => 'The field is required')
        } else {
            setEmail(email)
            setEmailError('')
        }
    }
    const isValid = (email) => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const passwordHandler = (e) => {
        const password = e.target.value;
        if (password && !isSecure(password)) {
            setPasswordError('The password should contain at least 8 symbols')
        } else if (!password) {
            setPasswordError('The field is required')
        } else {
            setPassword(e.target.value)
            setPasswordError('')
        }
    }

    const isSecure = (password) => {
        return !(password && password.length < 8);
    }

    const handler = (e) => {
        e.preventDefault()

        if (email && !emailError && password && !passwordError ) {
            loggedIn = setLoggedIn(true)
            if (checked) {
                localStorage.setItem('user', JSON.stringify({email: email, password: password}))}
        }
    }

    return (
        <div className={isActive === 1 ? 'form visible' :  'invisible'} id={'signing'}>
            <form className='innerForm' >
                <h1 className={'form__title'}> Sign into your account </h1>
                <div className='input emailInput'>
                    <label>
                        <input id={'email'}
                               type='text'
                               name={'email'}
                               placeholder='E-mail'
                               onBlur={event => emptyHandler(event)}
                               onChange={event => emailHandler(event)}
                        />
                    </label>
                    { emailError && <div>{emailError}. {/*{emailExist && <Link to={'/'}>Want to create an account?</Link>}*/}</div>}
                </div>
                <div className='input passwordInput'>
                    <label >
                        <input id={'password'}
                               type='password'
                               name={'password'}
                               placeholder='Password'
                               onBlur={event => emptyHandler(event)}
                               onChange={event => passwordHandler(event)}
                        />
                    </label>
                    { passwordError && <div>{passwordError}</div>}
                </div>
                <div className='checkboxWrapper'>
                    <input type='checkbox'
                           id='checkboxRemember'
                           onChange={event => setChecked(!checked)}
                    />
                    <label htmlFor='checkboxRemember'> Remember me </label>
                </div>
                <div className={'buttonWrapper'}>
                    <button className={'button'}
                            disabled={!(email && !emailError && password && !passwordError )}
                            onClick={(e) => handler(e)}> Log in </button>
                </div>
            </form>
        </div>
    )
}

export default SignInForm

import './Footer.css'
import {NavLink} from "react-router-dom";

const Footer = () => {
    return  (
        <div className='footer'>
           <ul>
               <li>
                   <NavLink to="about">About us</NavLink>
                   <span> · </span>
               </li>
               <li>
                   <NavLink to="support">FAQ</NavLink>
                   <span> · </span>
               </li>
               <li>
                   <NavLink to="feedback">Write to us</NavLink>
                   <span> · </span>
               </li>
               <li>
                   <NavLink to="commercials">Commercials</NavLink>
                   <span> · </span>
               </li>
               <li>
                   <NavLink to="/agreement">User agreement</NavLink>
               </li>
           </ul>

            <span>Made by Khasanova Alina, 2021</span>
        </div>
    )

}

export default Footer
import './Feedback.css'
import {Link} from "react-router-dom";
const Feedback = () => {
    return (
        <div className='companyInfo'>
            <h1>Feedback</h1>
            <h2 className='form__title'>Send us your feedback! And we will reach you back</h2>
            <form className='form__feedback' >
                <label htmlFor="feedback__name" className='feedback__label'> Your name
                    <input  type="name" id="feedback__name"/>
                </label>
                <label htmlFor="feedback__email" className='feedback__label'> Your email
                    <input  type="email" id="feedback__email"/>
                </label>
                <textarea
                    placeholder='Type your feedback or describe the usage problem here'
                    className='feedback__textarea'/>
                <button className='button feedback__button'> Send! </button>
            </form>
            <div className='feedback__notice'>In case you have some usage problems you can also check <Link to='/faq'>Frequently asked questions</Link></div>
        </div>
    )
}
export default Feedback

const Faq = () => {
    return (
        <div className='companyInfo'>
            <h1>Frequently asked questions</h1>
            <div>
                <h3>Problem 1</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?</p>
                <h3>Decision for problem 1</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?</p>
                <h3>Problem 2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?</p>
                <h3>Decision for problem 2</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?</p>
                <h3>Problem 3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?</p>
                <h3>Decision for problem 3</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquid assumenda aut corporis deserunt impedit
                    in ipsam, modi molestias nam nemo nobis quae quas quod, repudiandae tempora, vel voluptatum! Ad?</p>
            </div>
        </div>
    )
}
export default Faq
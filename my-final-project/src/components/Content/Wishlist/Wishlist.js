import WishlistItem from "./WishlistItem/WishlistItem";
import {useState} from "react";
import WishlistForm from "./WishlistForm/WishlistForm";
import './Wishlist.css';

const Wishlist = () => {
    const [wishes, setWishes] = useState([])
    const nextIdMaker = () => {
        let id = 0
        const helper = () => {
            return id++
        }
       return helper
    }
    const nextId = nextIdMaker();

    const addWish = (wish) => {
        if (wish) {
            const newWish = {
                id: nextId() + wish,
                text: wish,
            }
            setWishes([newWish, ...wishes])
            console.log(wishes)
        }
    }

    const removeWish = (id) => {
        setWishes([...wishes.filter((wish) => (wish.id !== id))])
    }


    return (
        <div className='wishlist'>
            <h2>My wishlist:</h2>
            <WishlistForm add={addWish}/>
            <ul className='wishlist__ul'>
                {wishes?.map(wish => (
                    <WishlistItem key={nextId() + wish.text} wish={wish} removeWish={removeWish}/>
                ))}
            </ul>

        </div>
    )

}

export default Wishlist
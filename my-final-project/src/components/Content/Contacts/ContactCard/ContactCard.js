import './ContactCard.css'
const ContactCard = ({id, name, email, phone, website,street, city, zipcode,company}) => {
    return(
        <div className='contactCard' >
            <h3>{name}</h3>
            <div>
                <div>Company: {company}</div>
                <div>Email: {email}</div>
                <div>Phone: {phone}</div>
                <div>Address: {`${city}, ${street}, ${zipcode}`}</div>
                <div>Website: {website}</div>
            </div>
        </div>
    )
}

export default ContactCard
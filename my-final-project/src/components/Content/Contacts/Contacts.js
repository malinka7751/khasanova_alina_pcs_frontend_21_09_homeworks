import {useEffect, useState} from "react";
import ContactCard from "./ContactCard/ContactCard";
import './Contacts.css'
import {CircularProgress} from "@mui/material";

const Contacts = () => {
    const [contacts, setContacts] = useState([])
    const [isFetching, setIsFetching] = useState(false)
    useEffect(() => {
        setIsFetching(true)
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(users => {
                setIsFetching(false)
                setContacts(users)
            })
            .catch(err => alert(`Oops! something went wrong! ${err.message}`))
    }, [])

    return (
        <>
            {isFetching ? <div  className='loading'><CircularProgress/></div> : null}
            <div className={'contacts'} >
                {contacts.map(contact => {
                    return <ContactCard key={contact.id}
                                        name={contact.name}
                                        email={contact.email}
                                        phone={contact.phone}
                                        website={contact.website}
                                        street={contact.address.street}
                                        city={contact.address.city}
                                        zipcode={contact.address.zipcode}
                                        company={contact.company.name}/>

                })}
            </div>
        </>)
}

export default Contacts
import './ContentNavigationItem.css'

const ContentNavigationItem = ({text, privat, last}) => {
    let className = 'contentNavigation__item';
    if (last) {
        className += ' contentNavigation__item_last'
    }
    if (privat) {
        className += ' contentNavigation__item_privat'
    }

    return (
        <div className={className}>
            <h4>{text}</h4>
        </div>
    )

}

export default ContentNavigationItem
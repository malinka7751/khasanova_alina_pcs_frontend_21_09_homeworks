import classes from './ContentNavigation.module.css'
import ContentNavigationItem from "./ContentNavigationItem/ContentNavigationItem";
import {NavLink} from "react-router-dom";


const ContentNavigation = () => {

    return (

        <div className={classes.wrapper}>
            <div className={classes.contentNavigation__itemWrapper}>
                <NavLink end to={''}
                         className={({isActive}) => isActive ? 'contentNavigation__item_active' : ''}>
                    <ContentNavigationItem text='My news'/>
                </NavLink>
            </div>
            <div className={classes.contentNavigation__itemWrapper}>
                <NavLink to={'education'} className={({isActive}) => isActive ? 'contentNavigation__item_active' : ''}>
                    <ContentNavigationItem text='Education'/>
                </NavLink>
            </div>
            <div className={classes.contentNavigation__itemWrapper}>
                <NavLink to={'wishlist'}
                         className={({isActive}) => isActive ? 'contentNavigation__item_active' : ''}>
                    <ContentNavigationItem text='Whishlist'/>
                </NavLink>
            </div>
            <div className={classes.contentNavigation__itemWrapper}>
                <NavLink to={'contacts'}
                         className={({isActive}) => isActive ? 'contentNavigation__item_active' : ''}>
                    <ContentNavigationItem text='Contacts' privat/></NavLink>
            </div>
            <div className={classes.contentNavigation__itemWrapper}>
                <NavLink to={'diary'}
                         className={({isActive}) => isActive ? 'contentNavigation__item_active' : ''}>
                    <ContentNavigationItem text='Diary' privat/>
                </NavLink>
            </div>
            <div className={classes.contentNavigation__itemWrapper}>
                <NavLink to={'calendar'}
                         className={({isActive}) => isActive ? 'contentNavigation__item_active' : ''}>
                    <ContentNavigationItem text='Calendar' privat last/>
                </NavLink>
            </div>
        </div>
    )
}

export default ContentNavigation


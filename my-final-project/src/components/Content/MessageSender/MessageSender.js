import './MessageSender.css'
import {Avatar} from "@mui/material";
import {useContext, useRef} from "react";
import {UserContext} from "../../../contexts/UserContext";

const MessageSender = ({sendPost}) => {
    const user = useContext(UserContext)
    const inputEl = useRef(null);
    const addPost = () => {
        const time = new Date().toLocaleString('en-US', {year:'numeric', month: 'long', day: 'numeric', weekday: 'long', hour:'numeric', minute: 'numeric'})
        if (inputEl.current.value) {
            const message = inputEl.current.value
            sendPost(prevState => {
                const newMessage = {
                    id: Date.now(),
                    message: message,
                    profile: user.avatar,
                    username: `${user.firstname} ${user.lastname}`,
                    time
                }
                console.log ([newMessage, ...prevState])
                inputEl.current.value = ''
                return [newMessage, ...prevState]

            })
        }
    }

    return (
        <div className='messageSender'>
            <div className='messageSender__wrapper'>
                <Avatar  src={user.avatar}/>
                <input className='messageSender__input'
                    ref={inputEl}
                    type={'text'}
                    placeholder={"Share what is up? Your friend are able to see"}/>
                <div className= 'form__button__wrapper'>
                    <button className={'button'} onClick={addPost}>Share!</button >
                </div>
            </div>
        </div>
    )
}

export default MessageSender
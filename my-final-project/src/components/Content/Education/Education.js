import {useContext} from "react";
import {UserContext} from "../../../contexts/UserContext";
import './Education.css';

const Education = () => {
    const user = useContext(UserContext)
    return (
        <div className="education">
            {user.education.higher && <div>
                <div><h3 className='education__title'>University:</h3> <p>{user.education.higher.university}</p></div>
                <div><h4 className='education__title'>Period of study:</h4>  <p>{`${user.education.higher.startStudy} - ${user.education.higher.endStudy}`}</p></div>
            </div>}
            {user.education.school && <div>
                <div><h3 className='education__title'>School:</h3>  <p>{`№ ${user.education.school.number}, city of ${user.education.school.city}`}</p></div>
                <div><h4 className='education__title'>Period of study:</h4>  <p>{`${user.education.school.startStudy} - ${user.education.school.endStudy}`}</p></div>
            </div>}
        </div>
    )
}

export default Education
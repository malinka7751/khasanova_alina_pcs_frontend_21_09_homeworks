
import {useEffect, useState} from "react";
import FriendCard from "./FriendCard/FriendCard";
import './FriendsList.css'
import {CircularProgress} from "@mui/material";



const FriendsList = () => {
    const [friends, setFriends] = useState([])
    const [isFetching, setIsFetching] = useState(false)
    let num = friends?.length
    useEffect(() => {
        setIsFetching(true)
        fetch('https://reqres.in/api/users')
            .then(response => response.json())
            .then(({total}) => {
                fetch(`https://reqres.in/api/users?per_page=${total}`)
                    .then(response => response.json())
                    .then(({data}) => {
                        setIsFetching(false)
                        setFriends(data)
                    })
                    .catch(err => alert(`Oops! something went wrong! ${err.message}`))
            })
            .catch(err => alert(`Oops! something went wrong! ${err.message}`))
    }, [])

    const removeFriend = (id) => {
        setFriends([...friends.filter((friend) => (friend.id !== id))])
    }
    console.log(isFetching)
    return (

         <div className={'userFriends'}>
                <h1>My friends: {num}</h1>
                {isFetching ? <div  className='loading'><CircularProgress/></div> : null}
                <div className={'friends__list'}>
                    {friends?.map(friend => (
                        <FriendCard key={friend.id}
                                    id={friend.id}
                                    first_name={friend.first_name}
                                    last_name={friend.last_name}
                                    avatar={friend.avatar}
                                    email={friend.email}
                                    removeFriend={removeFriend}
                        />))}

                </div>
         </div>)


}

export default FriendsList
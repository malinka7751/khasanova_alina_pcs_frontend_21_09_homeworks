import classes from './InfoLink.module.css'
const InfoLink = ({Icon, src, color, bgColor}) => {
    return (
        <div className={classes.info__link}  style={{backgroundColor: bgColor}}>
            <a href={src} rel='nofollow noopener noreferrer' target='_blank'>
                <Icon  className={classes.info__link__icon} style={{color: color, fontSize: 'xx-large'}}/>
            </a>
        </div>)
}

export default InfoLink